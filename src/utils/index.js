import firebase from 'react-native-firebase'

const messagesRef = firebase
  .database()
  .ref()
  .child('messages')

export function postFirebaseMessage(data) {
  messagesRef.push(data)
}

export function enableFirebaseMessagesListener(callback) {
  messagesRef.limitToLast(20).on('value', snapshot => {
    let messages = []
    snapshot.forEach(childSnapshot => {
      const message = childSnapshot.val()
      messages.unshift(message)
    })
    callback(messages)
  })
}

export function firebaseLogin() {
  return firebase.auth().signInAnonymously()
}
