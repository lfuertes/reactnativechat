import React from 'react'
import { SafeAreaView, View, ActivityIndicator } from 'react-native'
import styles from './styles'
import { GiftedChat } from 'react-native-gifted-chat'
import _ from 'lodash'
import * as firebaseUtils from '../../utils'

class App extends React.Component {
  constructor(props) {
    super(props)

    this.name = 'Luis'
    this.avatar = 'https://cdn.shopify.com/s/files/1/1123/6894/articles/alpaca-blog-hero-image_1400x.progressive.jpg'

    this.state = {
      uid: null,
      messages: []
    }
    this._login()
  }

  _login = () => {
    firebaseUtils
      .firebaseLogin()
      .then(res => {
        const uid = _.get(res, 'user._user.uid', null)
        this.setState({ uid })
        this._enableMessagesListener()
      })
      .catch(err => {
        this.setState({ uid: null })
      })
  }

  _enableMessagesListener = () => {
    firebaseUtils.enableFirebaseMessagesListener(this._updateMessagesList)
  }

  _updateMessagesList = messages => {
    this.setState({ messages })
  }

  _onSend = (messages = []) => {
    const message = _.first(messages)
    firebaseUtils.postFirebaseMessage(message)
  }

  _renderContent() {
    if (this.state.uid) {
      return (
        <GiftedChat
          messages={this.state.messages}
          onSend={messages => this._onSend(messages)}
          user={{
            _id: this.state.uid,
            name: this.name,
            avatar: this.avatar
          }}
        />
      )
    } else {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator color={'white'} />
        </View>
      )
    }
  }

  render() {
    return <SafeAreaView style={styles.container}>{this._renderContent()}</SafeAreaView>
  }
}

export default App
